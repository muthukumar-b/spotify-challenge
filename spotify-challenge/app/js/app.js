angular.module('SpotifyApp', [
  'SpotifyApp.services',
  'SpotifyApp.controllers',
  'ngRoute',
  'lrInfiniteScroll',
  'friendlyURL',
  'ngStorage'
]).

config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  	when("/artists", {templateUrl: "views/artists.html", controller: "artistsController"}).
  	when("/artists/:artist", {templateUrl: "views/artists.html", controller: "artistsController"}).
	when("/albums/:artist", {templateUrl: "views/albums.html", controller: "albumsController"}).
	when("/tracks/:album", {templateUrl: "views/tracks.html", controller: "tracksController"}).
  	when("/favorites", {templateUrl: "views/favorites.html", controller: "favoritesController"}).
	otherwise({redirectTo: '/artists'});
}]);

