angular.module('SpotifyApp.controllers', []).

controller('artistsController', ['$scope', '$timeout', 'spotifyAPIservice', '$routeParams', '$localStorage', function($scope, timer, spotifyAPIservice, $routeParams, $localStorage){
		
		$scope.getArtistDetails = function() {
			$scope.artistsData = []; $scope.counter = 0; $scope.keyword = $scope.artistName;
			spotifyAPIservice.getArtistDetails($scope.artistName).success(function (response) {
				$scope.artistsArr=response;
				$scope.loadMoreArtist();
			});
		}
		if($routeParams.artist!=null){
				$scope.artistsData = []; $scope.counter = 0; 
				spotifyAPIservice.getArtistDetails($routeParams.artist).success(function (response) {
				$scope.artistsArr=response;
				$scope.loadMoreArtist();
			});
		}
		$scope.loadMoreArtist = function () { 
			$scope.isLoading = true;
			for(i=0; i<12; i++){
				if($scope.counter==$scope.artistsArr.artists.items.length){$scope.isLoading = false; break; }
				$scope.artistsData.push($scope.artistsArr.artists.items[$scope.counter]);
				$scope.counter++; timer(function () { $scope.isLoading = false; }, 1000); 
			}
		};
		$scope.addArtist = function(artistName) {
			if($localStorage.data){
				  $localStorage.data = $localStorage.data.concat([{type:'artist', val:artistName}]);
			}else{
				 $localStorage.data = [{type:'artist', val:artistName}];
			}
		};					
}]).
	
controller('albumsController', ['$scope', '$timeout', '$routeParams', 'spotifyAPIservice', '$localStorage', function($scope, timer, $routeParams, spotifyAPIservice, $localStorage) {
			$scope.albumsData = []; $scope.counter = 0; $scope.artistName = $routeParams.artist;
			spotifyAPIservice.getAlbumDetails($routeParams.artist).success(function (response) {
				$scope.albumsArr=response;
				$scope.loadMoreAlbums();
			});
			$scope.loadMoreAlbums = function () { 
				$scope.isLoading = true;
				for(i=0; i<12; i++){
					if($scope.counter==$scope.albumsArr.albums.items.length){$scope.isLoading = false; break; }
					$scope.albumsData.push($scope.albumsArr.albums.items[$scope.counter]);
					$scope.counter++; timer(function () { $scope.isLoading = false; }, 1000); 
				}
			};
			$scope.addAlbum = function(albumName) {
				if($localStorage.data){
					  $localStorage.data = $localStorage.data.concat([{type:'album', val:albumName}]);
				}else{
					 $localStorage.data = [{type:'album', val:albumName}];
				}
			};					
}]).
	
controller('tracksController', ['$scope', '$routeParams', 'spotifyAPIservice', '$localStorage', function($scope, $routeParams, spotifyAPIservice, $localStorage) {
			spotifyAPIservice.gettrackDetails($routeParams.album).success(function (response) {
				$scope.tracksData=response.tracks.items; $scope.tracksOf = $routeParams.album;
			});
			$scope.addTracks = function(trackName) {
				if($localStorage.data){
					  $localStorage.data = $localStorage.data.concat([{type:'track', val:trackName}]);
				}else{
					 $localStorage.data = [{type:'track', val:trackName}];
				}
			};
				
}]).
controller('favoritesController', ['$scope', '$localStorage', function($scope, $localStorage) {
			$scope.storage = $localStorage;
			
			$scope.removeStorage = function(index) {
          		delete $localStorage.data[index];
	        };
}]);