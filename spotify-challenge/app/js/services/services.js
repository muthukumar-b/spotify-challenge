angular.module('SpotifyApp.services', [])
  .factory('spotifyAPIservice', ['$http', function($http) {

    var spotifyAPI = {};

	/* Get Artists */
    spotifyAPI.getArtistDetails = function(keyword) {
      return $http({
        method: 'get',
		url: 'https://api.spotify.com/v1/search?type=artist&limit=50&q='+keyword
      });
    }
	
	/* Get Albums */
    spotifyAPI.getAlbumDetails = function(keyword) {
      return $http({
        method: 'get',
		url: 'https://api.spotify.com/v1/search?type=album&limit=50&q='+keyword
      });
    }
	
	/* Get Tracks */
    spotifyAPI.gettrackDetails = function(keyword) {
      return $http({
        method: 'get',
		url: 'https://api.spotify.com/v1/search?type=track&limit=50&q='+keyword
      });
    }	
	

    return spotifyAPI;
  }]);