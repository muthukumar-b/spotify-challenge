angular.module('friendlyURL',[])
.filter('clean', function(){
return function (input){
  if(input){
  return input.toLowerCase()
    .replace(/\s+/g, '-')
    .replace(/[^a-z0-9-]/ig,'');
         }
       }
});